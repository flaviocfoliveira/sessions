package sessions

import (
	"context"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"testing"
	"time"
)

const conn string = "mongodb://auditUser:auditPass@192.168.1.99:27017/audit-tests"

func SetupMongodbRepository[T any]() (IRepository[T], TestsScope[T]) {

	opt := options.Client().ApplyURI(conn)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, opt)
	if err != nil {
		panic(err)
	}

	db := client.Database(opt.Auth.AuthSource)

	repo, err := NewMongodbRepository[T](db)
	if err != nil {
		log.Fatal(err)
	}

	return IRepository[T](repo), TestsScope[T]{
		collection: db.Collection("sessions"),
		IdList:     []string{},
	}
}

func TestMongodbRepository_CreateSession_success(t *testing.T) {

	// ARRANGE ------------------------------------
	repo, scope := SetupMongodbRepository[SESSION_DATA]()
	defer scope.Cleanup()

	obj := scope.GenerateSession(func() SESSION_DATA {
		return SESSION_DATA{
			ABC: "A",
			DEF: 3,
		}
	}, false)

	// ACT ----------------------------------------
	id, err := repo.CreateSession(obj)

	// ASSERT -------------------------------------
	assert.Nil(t, err)
	assert.NotEmpty(t, id)

	scope.IdList = append(scope.IdList, id)
}
func TestMongodbRepository_UpdateSession_success(t *testing.T) {

	// ARRANGE ------------------------------------
	repo, scope := SetupMongodbRepository[SESSION_DATA]()
	defer scope.Cleanup()

	obj := scope.GenerateSession(func() SESSION_DATA {
		return SESSION_DATA{
			ABC: "A",
			DEF: 3,
		}
	}, true)

	NewData := SESSION_DATA{
		ABC: "AAAA",
		DEF: 333,
	}

	// ACT ----------------------------------------
	err := repo.UpdateSession(obj.ID, NewData)

	// ASSERT -------------------------------------
	assert.Nil(t, err)

	session, err := scope.GetSession(obj.ID)
	assert.Nil(t, err)

	assert.Equal(t, NewData.ABC, session.Data.ABC)
	assert.Equal(t, NewData.DEF, session.Data.DEF)

}
func TestMongodbRepository_GetSession_success(t *testing.T) {

	// ARRANGE ------------------------------------
	repo, scope := SetupMongodbRepository[SESSION_DATA]()
	defer scope.Cleanup()

	obj := scope.GenerateSession(func() SESSION_DATA {
		return SESSION_DATA{
			ABC: "A",
			DEF: 3,
		}
	}, true)

	// ACT ----------------------------------------
	session, err := repo.GetSession(obj.ID)

	// ASSERT -------------------------------------
	assert.Nil(t, err)

	assert.Equal(t, obj.Data.ABC, session.Data.ABC)
	assert.Equal(t, obj.Data.DEF, session.Data.DEF)

}
func TestMongodbRepository_SetExpiration_success(t *testing.T) {

	// ARRANGE ------------------------------------
	repo, scope := SetupMongodbRepository[SESSION_DATA]()
	defer scope.Cleanup()

	obj := scope.GenerateSession(func() SESSION_DATA {
		return SESSION_DATA{
			ABC: "A",
			DEF: 3,
		}
	}, true)

	dt := time.Now().Add(time.Duration(5) * time.Minute).UTC()

	// ACT ----------------------------------------
	err := repo.SetExpiration(obj.ID, dt)

	// ASSERT -------------------------------------
	assert.Nil(t, err)

}
func TestMongodbRepository_Terminate_success(t *testing.T) {

	// ARRANGE ------------------------------------
	repo, scope := SetupMongodbRepository[SESSION_DATA]()
	defer scope.Cleanup()

	obj := scope.GenerateSession(func() SESSION_DATA {
		return SESSION_DATA{
			ABC: "A",
			DEF: 3,
		}
	}, true)

	// ACT ----------------------------------------
	err := repo.Terminate(obj.ID)

	// ASSERT -------------------------------------
	assert.Nil(t, err)

	r, err := scope.GetSession(obj.ID)
	assert.Nil(t, err)
	assert.Nil(t, r)

}
func TestMongodbRepository_Exists_success(t *testing.T) {

	// ARRANGE ------------------------------------
	repo, scope := SetupMongodbRepository[SESSION_DATA]()
	defer scope.Cleanup()

	obj := scope.GenerateSession(func() SESSION_DATA {
		return SESSION_DATA{
			ABC: "A",
			DEF: 3,
		}
	}, true)

	// ACT ----------------------------------------
	exists, err := repo.Exists(obj.ID)
	notExists, err := repo.Exists("62ddd71a822bac77485fa5c0")

	// ASSERT -------------------------------------
	assert.Nil(t, err)
	assert.True(t, exists)
	assert.False(t, notExists)

}

type TestsScope[T any] struct {
	collection *mongo.Collection
	IdList     []string
}

func (ts *TestsScope[T]) GetSession(sessionID string) (*Session[T], error) {
	id, err := primitive.ObjectIDFromHex(sessionID)
	if err != nil {
		return nil, err
	}

	filter := bson.M{"_id": id}

	dbRes := ts.collection.FindOne(context.Background(), filter)
	if err := dbRes.Err(); err != nil {
		if mongo.ErrNoDocuments == err {
			return nil, nil
		}
		return nil, err
	}

	var r Session[T]
	if err := dbRes.Decode(&r); err != nil {
		return nil, err
	}

	return &r, nil
}
func (ts *TestsScope[T]) GenerateSession(builder GetInstance[T], insertDb bool) Session[T] {

	r := Session[T]{}
	r.Created = time.Now().UTC()
	r.Updated = time.Now().UTC()
	r.Expires = time.Now().Add(10 * time.Minute).UTC()

	if builder != nil {
		r.Data = builder()
	}

	if insertDb {
		inserted, err := ts.collection.InsertOne(context.Background(), r)
		if err != nil {
			panic(err)
		}
		r.ID = inserted.InsertedID.(primitive.ObjectID).Hex()
		ts.IdList = append(ts.IdList, r.ID)
	}

	return r
}
func (ts *TestsScope[T]) Cleanup() {
	if len(ts.IdList) > 0 {

		for _, v := range ts.IdList {

			oid, _ := primitive.ObjectIDFromHex(v)
			ts.collection.DeleteOne(context.Background(), bson.M{"_id": oid})

		}

	}
}

type GetInstance[T any] func() T

type SESSION_DATA struct {
	ABC string
	DEF int
}
