package sessions

//
//import (
//	"context"
//	"fmt"
//	"time"
//
//	"go.mongodb.org/mongo-driver/bson"
//	"go.mongodb.org/mongo-driver/mongo"
//	"go.mongodb.org/mongo-driver/mongo/options"
//)
//
//type MongodbStore struct {
//	settings   Settings
//	client     *mongo.Client
//	db         *mongo.Database
//	collection *mongo.Collection
//}
//
//func NewMongodbStore(s Settings) (*MongodbStore, error) {
//
//	r := MongodbStore{
//		settings: s,
//	}
//
//	// Connect to the server
//	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(s.ConnectionString))
//	if err != nil {
//		return nil, err
//	}
//	r.client = client
//
//	// Gets the database
//	db := client.Database(s.DatabaseName)
//	r.db = db
//
//	// Gets the collection
//	col := db.Collection(s.CollectionName)
//	r.collection = col
//
//	ttl := mongo.IndexModel{
//		Keys:    bson.M{"Expires": 1},
//		Options: options.Index().SetName(fmt.Sprintf("TTL-%s-Expires", s.CollectionName)).SetExpireAfterSeconds(0),
//	}
//	_, err = col.Indexes().CreateOne(context.Background(), ttl)
//	if err != nil {
//		return nil, err
//	}
//
//	return &r, nil
//}
//
//func (m MongodbStore) CreateSession(sessionData Session) (string, error) {
//
//	isValid := false
//	for !isValid {
//		sessionData.ID = GenerateSessionID(32)
//
//		exist, err := m.Exists(sessionData.ID)
//		if err != nil {
//			return "", err
//		}
//
//		isValid = !exist
//	}
//
//	sessionData.Created = time.Now().UTC()
//	sessionData.Updated = time.Now().UTC()
//	sessionData.Expires = time.Now().Add(time.Duration(m.settings.TTLMinutes) * time.Minute).UTC()
//
//	_, err := m.collection.InsertOne(context.Background(), sessionData)
//	if err != nil {
//		return "", err
//	}
//
//	return sessionData.ID, nil
//}
//*/
