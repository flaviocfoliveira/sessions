package sessions

import (
	"time"
)

type Settings struct {
	TTLMinutes        int
	SlidingExpiration bool
}

type Session[T any] struct {
	ID      string    `bson:"_id,omitempty"`
	Data    T         `bson:"Data,omitempty"`
	Created time.Time `bson:"Created,omitempty"`
	Updated time.Time `bson:"Updated,omitempty"`
	Expires time.Time `bson:"Expires,omitempty"`
}
