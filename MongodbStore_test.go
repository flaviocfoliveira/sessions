package sessions

//
//import (
//	"context"
//	"testing"
//	"time"
//
//	"go.mongodb.org/mongo-driver/bson"
//	"go.mongodb.org/mongo-driver/mongo"
//)
//
//const (
//	// conn string = "mongodb://usr:000000@10.2.2.21:27017/Sessions"
//	conn string = "mongodb://usr:000000@192.168.1.89:27017/Sessions"
//)
//
//func TestMongodbStore_CreateSession_SUCCESS(t *testing.T) {
//	// ARRANGE ------------------------------------
//	scope := setup(t)
//	s := scope.generateNewInstance(false)
//
//	// ACT ----------------------------------------
//	sessionID, err := scope.Store.CreateSession(s)
//	if err != nil {
//		t.Fatal(err)
//	}
//	scope.SessionIDList = append(scope.SessionIDList, sessionID)
//
//	// ASSERT -------------------------------------
//	sess := scope.getDBSession(sessionID)
//	if sess == nil {
//		scope.Cleanup()
//		t.Fatal("Session was not created")
//	}
//
//	scope.Cleanup()
//}
//
//func TestMongodbStore_Exists(t *testing.T) {
//	// ARRANGE ------------------------------------
//	scope := setup(t)
//	s := scope.generateNewInstance(true)
//
//	// ACT ----------------------------------------
//	vTrue, errTrue := scope.Store.Exists(s.ID)
//	vFalse, errFalse := scope.Store.Exists("NOT.EXIST.ID")
//
//	// ASSERT -------------------------------------
//	if errTrue != nil {
//		scope.Cleanup()
//		t.Fatal(errTrue)
//	}
//	if errFalse != nil {
//		scope.Cleanup()
//		t.Fatal(errFalse)
//	}
//
//	if vTrue == false {
//		scope.Cleanup()
//		t.Fatal("The Response should be true")
//	}
//
//	if vFalse == true {
//		scope.Cleanup()
//		t.Fatal("The Response should be False")
//	}
//
//	scope.Cleanup()
//}
//
//func TestMongodbStore_GetSession(t *testing.T) {
//	// ARRANGE ------------------------------------
//	scope := setup(t)
//	s := scope.generateNewInstance(true)
//
//	// ACT ----------------------------------------
//	vTrue1, errTrue1 := scope.Store.GetSession(s.ID, false)
//	vFalse1, errFalse1 := scope.Store.GetSession("NOT.EXIST.ID", false)
//
//	vTrue2, errTrue2 := scope.Store.GetSession(s.ID, true)
//	vFalse2, errFalse2 := scope.Store.GetSession("NOT.EXIST.ID", true)
//
//	// ASSERT -------------------------------------
//	if errTrue1 != nil {
//		scope.Cleanup()
//		t.Fatal(errTrue1)
//	}
//	if errFalse1 != nil {
//		scope.Cleanup()
//		t.Fatal(errFalse1)
//	}
//	if errTrue2 != nil {
//		scope.Cleanup()
//		t.Fatal(errTrue2)
//	}
//	if errFalse2 != nil {
//		scope.Cleanup()
//		t.Fatal(errFalse2)
//	}
//
//	if vTrue1 == nil {
//		scope.Cleanup()
//		t.Fatal("The Response should be true")
//	}
//	if vTrue2 == nil {
//		scope.Cleanup()
//		t.Fatal("The Response should be true")
//	}
//
//	if vFalse1 != nil {
//		scope.Cleanup()
//		t.Fatal("The Response should be False")
//	}
//	if vFalse2 != nil {
//		scope.Cleanup()
//		t.Fatal("The Response should be False")
//	}
//
//	scope.Cleanup()
//}
//
//func TestMongodbStore_Terminate(t *testing.T) {
//
//	// ARRANGE ------------------------------------
//	scope := setup(t)
//	s := scope.generateNewInstance(true)
//
//	// ACT ----------------------------------------
//	err := scope.Store.Terminate(s.ID)
//
//	// ASSERT -------------------------------------
//	if err != nil {
//		scope.Cleanup()
//		t.Fatal(err)
//	}
//
//	sess := scope.getDBSession(s.ID)
//	if sess != nil {
//		scope.Cleanup()
//		t.Fatal("Session was not terminated")
//	}
//
//	scope.Cleanup()
//}
//
///*
//
//
//AUX
//
//
//*/
//type testScope struct {
//	T             *testing.T
//	Config        *Settings
//	Store         *MongodbStore
//	SessionIDList []string
//}
//
//func (this *testScope) generateNewInstance(insertOnDB bool) Session {
//	r := Session{
//		ID:        GenerateSessionID(this.Config.TokenSize),
//		PublicID:  GenerateSessionID(8),
//		IPAddress: "192.168.1.11",
//		Created:   time.Now(),
//		Updated:   time.Now(),
//		Expires:   time.Now().Add(time.Duration(this.Config.TTLMinutes) * time.Minute).UTC(),
//	}
//
//	if insertOnDB {
//		this.addToDatabase(r)
//	}
//
//	return r
//}
//func (this *testScope) addToDatabase(s Session) {
//
//	_, err := this.Store.collection.InsertOne(context.TODO(), s)
//	if err != nil {
//		panic(err)
//	}
//
//	this.SessionIDList = append(this.SessionIDList, s.ID)
//}
//func (this *testScope) getDBSession(sessionID string) *Session {
//
//	dbRes := this.Store.collection.FindOne(context.TODO(), bson.M{"_id": sessionID})
//	if err := dbRes.Err(); err != nil {
//		if mongo.ErrNoDocuments == err {
//			return nil
//		}
//		panic(err)
//	}
//
//	var u Session
//	err := dbRes.Decode(&u)
//	if err != nil {
//		panic(err)
//	}
//
//	return &u
//}
//
//func setup(t *testing.T) *testScope {
//	r := testScope{}
//
//	r.T = t
//
//	r.Config = &Settings{
//		TokenSize:         24,
//		TTLMinutes:        1,
//		SlidingExpiration: true,
//		ConnectionString:  conn,
//		DatabaseName:      "Sessions",
//		CollectionName:    "Active",
//	}
//
//	m, err := NewMongodbStore(*r.Config)
//	if err != nil {
//		t.Fatalf("Error during test setup: %s", err)
//	}
//	r.Store = m
//
//	return &r
//}
//func (this testScope) Cleanup() {
//
//	if len(this.SessionIDList) > 0 {
//		for _, sessionID := range this.SessionIDList {
//			_, err := this.Store.collection.DeleteOne(context.Background(), bson.M{"_id": sessionID})
//			if err != nil {
//				this.T.Fatal(err)
//			}
//		}
//	}
//
//}
//
//// // ARRANGE ------------------------------------
//// scope := setup(t)
////
//// // ACT ----------------------------------------
////
////
//// // ASSERT -------------------------------------
////
//// scope.Cleanup()
