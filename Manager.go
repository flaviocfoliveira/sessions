package sessions

import (
	"time"
)

type Manager[T any] struct {
	settings Settings
	repo     IRepository[T]
}

func NewManager[T any](settings Settings, repository IRepository[T]) Manager[T] {
	return Manager[T]{
		settings: settings,
		repo:     repository,
	}
}

func (s Manager[T]) Create(data T) (string, error) {

	dt := time.Now().UTC()

	newSession := Session[T]{
		Data:    data,
		Created: dt,
		Updated: dt,
		Expires: time.Now().Add(time.Duration(s.settings.TTLMinutes) * time.Minute).UTC(),
	}

	return s.repo.CreateSession(newSession)
}

func (s Manager[T]) Update(id string, data T) error {
	return s.repo.UpdateSession(id, data)
}

func (s Manager[T]) Get(id string, slide bool) (*Session[T], error) {

	if slide && s.settings.SlidingExpiration {
		err := s.repo.SetExpiration(id, time.Now().Add(time.Duration(s.settings.TTLMinutes)*time.Minute).UTC())
		if err != nil {
			return nil, err
		}
	}

	return s.repo.GetSession(id)
}

func (s Manager[T]) Terminate(id string) error {
	return s.repo.Terminate(id)
}

func (s Manager[T]) Exists(id string) (bool, error) {
	return s.repo.Exists(id)
}
