package sessions

import "time"

type IRepository[T any] interface {
	CreateSession(sessionData Session[T]) (string, error)
	UpdateSession(sessionID string, data T) error
	GetSession(sessionID string) (*Session[T], error)
	SetExpiration(sessionID string, exp time.Time) error
	Terminate(sessionID string) error
	Exists(sessionID string) (bool, error)
}

type IManager[T any] interface {
	Create(data T) (string, error)
	Update(id string, data T) error
	Get(id string, slide bool) (*Session[T], error)
	Terminate(id string) error
	Exists(id string) (bool, error)
}
