package sessions

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

type MongodbRepository[T any] struct {
	collection *mongo.Collection
}

func NewMongodbRepository[T any](db *mongo.Database) (IRepository[T], error) {

	r := MongodbRepository[T]{}

	r.collection = db.Collection("sessions")

	ttl := mongo.IndexModel{
		Keys:    bson.D{{"Expires", 1}},
		Options: options.Index().SetName("ttl-session-expires").SetExpireAfterSeconds(0),
	}
	_, err := r.collection.Indexes().CreateOne(context.Background(), ttl)
	if err != nil {
		return nil, err
	}

	return r, err
}

func (m MongodbRepository[T]) UpdateSession(sessionID string, data T) error {

	id, err := primitive.ObjectIDFromHex(sessionID)
	if err != nil {
		return err
	}

	f := bson.M{"_id": id}
	u := bson.D{{"$set", bson.M{"Data": data, "Updated": time.Now().UTC()}}}

	_, err = m.collection.UpdateOne(context.Background(), f, u)
	if err != nil {
		return err
	}

	return nil
}

func (m MongodbRepository[T]) CreateSession(sessionData Session[T]) (string, error) {

	sessionData.ID = ""

	inserted, err := m.collection.InsertOne(context.Background(), sessionData)
	if err != nil {
		return "", err
	}

	sessionData.ID = inserted.InsertedID.(primitive.ObjectID).Hex()

	return sessionData.ID, nil
}

func (m MongodbRepository[T]) GetSession(sessionID string) (*Session[T], error) {

	id, err := primitive.ObjectIDFromHex(sessionID)
	if err != nil {
		return nil, err
	}

	filter := bson.M{"_id": id}

	dbRes := m.collection.FindOne(context.Background(), filter)
	if err := dbRes.Err(); err != nil {
		if mongo.ErrNoDocuments == err {
			return nil, nil
		}
		return nil, err
	}

	var r Session[T]
	if err := dbRes.Decode(&r); err != nil {
		return nil, err
	}

	return &r, nil
}

func (m MongodbRepository[T]) SetExpiration(sessionID string, exp time.Time) error {

	id, err := primitive.ObjectIDFromHex(sessionID)
	if err != nil {
		return err
	}

	f := bson.M{"_id": id}
	u := bson.D{{"$set", bson.M{"Expires": exp, "Updated": time.Now().UTC()}}}

	_, err = m.collection.UpdateOne(context.Background(), f, u)
	if err != nil {
		return err
	}

	return nil
}

func (m MongodbRepository[T]) Terminate(sessionID string) error {

	id, err := primitive.ObjectIDFromHex(sessionID)
	if err != nil {
		return err
	}

	_, err = m.collection.DeleteOne(context.Background(), bson.M{"_id": id})
	if err != nil {
		return err
	}

	return nil
}

func (m MongodbRepository[T]) Exists(sessionID string) (bool, error) {

	id, err := primitive.ObjectIDFromHex(sessionID)
	if err != nil {
		return false, err
	}

	opt := options.CountOptions{}
	opt.SetLimit(1)

	count, err := m.collection.CountDocuments(context.Background(), bson.M{"_id": id}, &opt)
	if err != nil {
		return false, err
	}

	return count > 0, nil
}
