package sessions

import (
	"context"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"testing"
	"time"
)

func Test_SessionsManager_Create_success(t *testing.T) {

	// ARRANGE ------------------------------------
	m, scope := SetupManager[SESSION_DATA]()
	defer scope.Cleanup()

	// ACT ----------------------------------------
	id, err := m.Create(SESSION_DATA{
		ABC: "123",
		DEF: 987,
	})

	// ASSERT -------------------------------------
	assert.Nil(t, err)
	assert.NotEmpty(t, id)
	scope.IdList = append(scope.IdList, id)

	session, err := scope.GetSession(id)
	assert.Nil(t, err)
	assert.NotNil(t, session)

	assert.Equal(t, "123", session.Data.ABC)
	assert.Equal(t, 987, session.Data.DEF)

}
func Test_SessionsManager_Update_success(t *testing.T) {

	// ARRANGE ------------------------------------
	m, scope := SetupManager[SESSION_DATA]()
	defer scope.Cleanup()

	obj := scope.GenerateSession(func() SESSION_DATA {
		return SESSION_DATA{
			ABC: "123",
			DEF: 987,
		}
	}, true)

	// ACT ----------------------------------------
	err := m.Update(obj.ID, SESSION_DATA{
		ABC: "12345",
		DEF: 98765,
	})

	// ASSERT -------------------------------------
	assert.Nil(t, err)

	session, err := scope.GetSession(obj.ID)
	assert.Nil(t, err)
	assert.NotNil(t, session)

	assert.Equal(t, "12345", session.Data.ABC)
	assert.Equal(t, 98765, session.Data.DEF)

}
func Test_SessionsManager_Get_success(t *testing.T) {

	// ARRANGE ------------------------------------
	m, scope := SetupManager[SESSION_DATA]()
	defer scope.Cleanup()

	obj := scope.GenerateSession(func() SESSION_DATA {
		return SESSION_DATA{
			ABC: "123",
			DEF: 987,
		}
	}, true)

	// ACT ----------------------------------------
	ss, err := m.Get(obj.ID, true)

	// ASSERT -------------------------------------
	assert.Nil(t, err)
	assert.NotNil(t, ss)

	assert.Equal(t, obj.ID, ss.ID)
	assert.Equal(t, "123", ss.Data.ABC)
	assert.Equal(t, 987, ss.Data.DEF)

}
func Test_SessionsManager_Terminate_success(t *testing.T) {

	// ARRANGE ------------------------------------
	m, scope := SetupManager[SESSION_DATA]()
	defer scope.Cleanup()

	obj := scope.GenerateSession(func() SESSION_DATA {
		return SESSION_DATA{
			ABC: "123",
			DEF: 987,
		}
	}, true)

	// ACT ----------------------------------------
	err := m.Terminate(obj.ID)

	// ASSERT -------------------------------------
	assert.Nil(t, err)

	session, err := scope.GetSession(obj.ID)
	assert.Nil(t, err)
	assert.Nil(t, session)

}
func Test_SessionsManager_Exists_success(t *testing.T) {

	// ARRANGE ------------------------------------
	m, scope := SetupManager[SESSION_DATA]()
	defer scope.Cleanup()

	obj := scope.GenerateSession(func() SESSION_DATA {
		return SESSION_DATA{
			ABC: "123",
			DEF: 987,
		}
	}, true)

	// ACT ----------------------------------------
	exists, err := m.Exists(obj.ID)
	assert.Nil(t, err)

	notExists, err := m.Exists("62ddd71a822bac77485fa5c0")
	assert.Nil(t, err)

	// ASSERT -------------------------------------

	assert.True(t, exists)
	assert.False(t, notExists)
}

func SetupManager[T any]() (IManager[T], TestsScope[T]) {

	opt := options.Client().ApplyURI(conn)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, opt)
	if err != nil {
		panic(err)
	}

	db := client.Database(opt.Auth.AuthSource)

	repo, err := NewMongodbRepository[T](db)
	if err != nil {
		log.Fatal(err)
	}

	m := NewManager[T](Settings{
		TTLMinutes:        15,
		SlidingExpiration: true,
	}, repo)

	return m, TestsScope[T]{
		collection: db.Collection("sessions"),
		IdList:     []string{},
	}
}
